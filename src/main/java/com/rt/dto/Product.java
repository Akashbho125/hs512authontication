package com.rt.dto;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Product {
		 private float id;
		 private String title;
		 private String description;
		 private float price;
		 private float discountPercentage;
		 private float rating;
		 private float stock;
		 private String brand;
		 private String category;
		 private String thumbnail;
		 private String[] images;
		public float getId() {
			return id;
		}
		public void setId(float id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public float getPrice() {
			return price;
		}
		public void setPrice(float price) {
			this.price = price;
		}
		public float getDiscountPercentage() {
			return discountPercentage;
		}
		public void setDiscountPercentage(float discountPercentage) {
			this.discountPercentage = discountPercentage;
		}
		public float getRating() {
			return rating;
		}
		public void setRating(float rating) {
			this.rating = rating;
		}
		public float getStock() {
			return stock;
		}
		public void setStock(float stock) {
			this.stock = stock;
		}
		public String getBrand() {
			return brand;
		}
		public void setBrand(String brand) {
			this.brand = brand;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public String getThumbnail() {
			return thumbnail;
		}
		public void setThumbnail(String thumbnail) {
			this.thumbnail = thumbnail;
		}
		public String[] getImages() {
			return images;
		}
		public void setImages(String[] images) {
			this.images = images;
		}



}
