package com.rt.dto;

import lombok.Data;

@Data
public class UserDetails {
    private long count;
    private String name;
    private String gender;
    private long probability;
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getProbability() {
		return probability;
	}
	public void setProbability(long probability) {
		this.probability = probability;
	}
}
